## Para ejecutar en local

- Luegos de clonar el proyecto ejecutar los comandos:
- composer install
- copiar el contenido del .env-example y pegarlo en un archivo .env
- crear la base de datos en local.
- en el .env poner el nombre de tu base de datos en la variable DB_DATABASE
- ejecuta php artisan migrate

