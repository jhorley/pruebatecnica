<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fecha extends Model
{
    // use HasFactory;

    protected $primaryKey = "fecha_id";
    protected $fillable = [
        'fecha',
        'dias',
        'final',
        'created_at',
        'updated_at'
    ];
}
