<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style> 
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
    <body class="antialiased" >
        <div class="content" id="prueba">
            <div class="row justify-content-center">
                <h1>Prueba técnica</h1>
            </div>


            <form>
                <div class="row">
                    <div class="col-6">
                        <label for="">Fecha</label>
                        <input type="date" class="form-control" v-model="fecha" require>
                    </div>
                    <div class="col-6">
                        <label for="">Días</label>
                        <input type="number" min="0" class="form-control" v-model="dias" require>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button @click="saveFecha()" :disabled="btnDisabled" class="btn btn-outline-primary mt-4">Guardar</button>
                </div>
            </form>
            <hr>

            <div class="row justify-content-center">
                <h2>Fechas</h2>
            </div>
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Fecha</th>
                    <th>días</th>
                    <th>Fecha final</th>
                </thead>
                <tbody>
                    <tr v-for="element in fechas"> 
                        <td v-text="element.fecha_id"></td>
                        <td v-text="element.fecha"></td>
                        <td v-text="element.dias"></td>
                        <td v-text="element.final"></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        let elemento = new Vue({
            el: '#prueba',
            mounted: function () {
                this.getFechas()
            },
            data: {
                fechas:[],
                fecha:null,
                dias:null,
                btnDisabled:false,
            },
            methods: {
                getFechas(){
                    let me=this;
                    axios.get('fechas').then(resp=>{
                        let answer=resp.data;
                        me.fechas=answer.fechas;
                        console.log(me.fechas);
                    }).catch(error=>{
                        console.error(error);
                    })
                },
                saveFecha(){
                    let me=this;
                    if(!me.fechas|| !me.dias ){
                        alert('faltan campos');
                    }else if(me.dias<0){
                        alert('no se permiten número negativos');
                    }else{
                        me.btnDisabled=true;
                        axios.post('fechas',{
                            'fecha':me.fecha,
                            'dias':me.dias
                        }).then(resp=>{
                            me.btnDisabled=false;
                            console.log('guardado exitoso');
                        }).catch(error=>{
                            me.btnDisabled=false;
                            console.error(error);
                        })
                    }
                }
            },
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>
